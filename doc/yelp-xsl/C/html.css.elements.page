<?xml version="1.0"?>
<page xmlns="http://projectmallard.org/1.0/" id="html.css.elements" type="topic" style="xslt-template"><info><link type="guide" xref="html" group="templates"/><link type="guide" xref="templates" group="html"/><revision version="1.0" date="2010-05-25" status="final"/><desc>Output CSS for common elements from source formats.</desc></info><title>html.css.elements</title><p>Output CSS for common elements from source formats.</p>
  
  
  <synopsis><title>Parameters</title><terms>
    <item>
      <title><code>node</code></title>
      <p>The node to create CSS for.</p>
    </item>
    <item>
      <title><code>direction</code></title>
      <p>The directionality of the text, either <code>ltr</code> or <code>rtl</code>.</p>
    </item>
    <item>
      <title><code>left</code></title>
      <p>The starting alignment, either <code>left</code> or <code>right</code>.</p>
    </item>
    <item>
      <title><code>right</code></title>
      <p>The ending alignment, either <code>left</code> or <code>right</code>.</p>
    </item>
  </terms></synopsis>
  <p>This template outputs CSS for elements from source languages like DocBook and Mallard. It defines them using common class names. The common names are often the simpler element names from Mallard, although there some class names which are not taken from Mallard. Stylesheets which convert to HTML should use the appropriate common classes.</p>
  <p>All parameters can be automatically computed if not provided.</p>
<list style="compact"><title>Calls Templates</title><item><p><link xref="l10n.direction"/></p></item><item><p><link xref="l10n.align.start"/></p></item><item><p><link xref="l10n.gettext"/></p></item></list><list style="compact"><title>Calls Parameters</title><item><p><link xref="color.bg"/></p></item><item><p><link xref="color.bg.blue"/></p></item><item><p><link xref="color.bg.dark"/></p></item><item><p><link xref="color.bg.gray"/></p></item><item><p><link xref="color.bg.yellow"/></p></item><item><p><link xref="color.blue"/></p></item><item><p><link xref="color.fg"/></p></item><item><p><link xref="color.fg.blue"/></p></item><item><p><link xref="color.fg.dark"/></p></item><item><p><link xref="color.fg.gray"/></p></item><item><p><link xref="color.fg.red"/></p></item><item><p><link xref="color.fg.yellow"/></p></item><item><p><link xref="color.gray"/></p></item><item><p><link xref="color.red"/></p></item><item><p><link xref="color.yellow"/></p></item><item><p><link xref="icons.size.quote"/></p></item></list></page>
