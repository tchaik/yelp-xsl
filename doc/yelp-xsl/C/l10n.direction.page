<?xml version="1.0"?>
<page xmlns="http://projectmallard.org/1.0/" id="l10n.direction" type="topic" style="xslt-template"><info><link type="guide" xref="l10n" group="templates"/><link type="guide" xref="templates" group="l10n"/><revision version="3.18" date="2015-08-13" status="final"/><desc>Determine the text direction for a language.</desc></info><title>l10n.direction</title><p>Determine the text direction for a language.</p>
  
  
  <synopsis><title>Parameters</title><terms>
    <item>
      <title><code>lang</code></title>
      <p>The locale to use to determine the text direction.</p>
    </item>
  </terms></synopsis>
  <p>This template returns the text direction for the language <code style="xslt-param">lang</code>. It returns <code>"ltr"</code> for left-to-right languages and <code>"rtl"</code> for right-to-left languages. If <code style="xslt-param">lang</code> is not provided, it defaults to <code style="xslt-param">l10n.locale</code>, the top-level locale of the document.</p>
  <p>This template calls <code style="xslt-template" xref="l10n.gettext">l10n.gettext</code> with the string <code>default:LTR</code> in the domain <code>yelp-xsl</code>. The language is right-to-left if the string <code>default:RTL</code> is returned. Otherwise, it is left-to-right. (This particular string is used to match the string used in GTK+, enabling translation memory.)</p>
<list style="compact"><title>Calls Templates</title><item><p><link xref="l10n.gettext"/></p></item></list><list style="compact"><title>Calls Parameters</title><item><p><link xref="l10n.locale"/></p></item></list></page>
