<?xml version="1.0"?>
<page xmlns="http://projectmallard.org/1.0/" id="html.syntax.class" type="topic" style="xslt-template"><info><link type="guide" xref="html" group="templates"/><link type="guide" xref="templates" group="html"/><revision version="3.28" date="2016-01-03" status="final"/><desc>Output HTML class values for syntax highlighting.</desc></info><title>html.syntax.class</title><p>Output HTML class values for syntax highlighting.</p>
  
  
  <synopsis><title>Parameters</title><terms>
    <item>
      <title><code>node</code></title>
      <p>The source element whose content will be syntax highlighted.</p>
    </item>
  </terms></synopsis>
  <p>This template calls <code style="xslt-mode" xref="html.syntax.class.mode">html.syntax.class.mode</code> on <code style="xslt-param">node</code>. If the result of that mode is a suitable language identifier, it outputs appropriate CSS class names to enable syntax highlighting. The output should be placed in the <code>class</code> attribute of a <code>pre</code> or similar output element by the calling template.</p>
  <p>Importing stylesheets should implement <code style="xslt-mode" xref="html.syntax.class.mode">html.syntax.class.mode</code> for any source elements that may be syntax highlighted, then call this template when building the <code>class</code> attribute for output elements.</p>
<list style="compact"><title>Calls Modes</title><item><p><link xref="html.syntax.class.mode"/></p></item></list></page>
